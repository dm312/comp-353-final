/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Danny
 */
public class ReadFile {

    public ReadFile(String file_path) {
        path = file_path;
    }

    public ArrayList<ArrayList<String>> OpenFile(int numberOfLines) throws IOException {
        FileReader fr = new FileReader(path);
        BufferedReader textReader = new BufferedReader(fr);

        String aLine;
        String[] textData = new String[numberOfLines];
        textReader.close();

        ArrayList<ArrayList<String>> textData2 = new ArrayList<ArrayList<String>>();
        ArrayList<String> tmp = new ArrayList<String>();
        //if it does not contain "E" then add, so when an E appears use it as a key and keep adding as "E" does not exist
        String key;

        for (int i = 0; i < textData.length; i++) {
            if (textData[i].startsWith("E")) {
                tmp.add(textData[i]);
                for (int j = i; j < textData.length; j++) {
                    tmp.add(textData[j]);
                    int e = j;
                    if (textData[i].startsWith("E")) {
                        break;
                    }
                }
            }
        }
        return textData2;
    }

    int readLines() throws IOException {
        FileReader file_to_read = new FileReader(path);
        BufferedReader bf = new BufferedReader(file_to_read);

        String aLine;
        int numberOfLines = 0;
        while ((aLine = bf.readLine()) != null) {
            numberOfLines++;
        }
        bf.close();
        return numberOfLines;
    }

    public void connect() {
        MyFunctions test = new MyFunctions();
        con = test.global_con();
        try {
            if (con.isValid(con.getNetworkTimeout())) {
                JOptionPane.showMessageDialog(null, "Connected");
            }
        } catch (Exception e) {
        }
    }

    public void disconnect() {
        con = null;
        JOptionPane.showMessageDialog(null, "Disconnected");
    }

    public void printTotalVotes(int userId) {
        String table = "Votes";
        if (this.exist(table, "voter_id", String.valueOf(userId)).equals("0")) {
            System.out.println("User: " + userId + " not found.");
        } else {
            try {
                Statement st = con.createStatement();
                String sql = "SELECT COUNT(voter_id) as votes FROM Votes where voter_id ='" + userId + "'";
                ResultSet rs = st.executeQuery(sql);
                int numberOfVotes = rs.getInt("votes");

                System.out.println("Number of votes: " + numberOfVotes);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void printTotalVotes(String screenname) {
        String table = "Votes";
        if (this.exist(table, "voter_id", screenname).equals("0")) {
            System.out.println("User: " + screenname + " not found.");
        } else {
            try {
                Statement st = con.createStatement();
                String sql = "SELECT COUNT(voter_id) as votes FROM Votes WHERE voter_id in (SELECT uno FROM User where uname ='" + screenname + "'";
                ResultSet rs = st.executeQuery(sql);
                int numberOfVotes = rs.getInt("votes");
                if (numberOfVotes == 0) {
                    System.out.println(screenname + " has not voted.");
                }
                System.out.println("Number of votes: " + numberOfVotes);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void printCredentials(int userId) {
        String table = "Election_Details";
        if (this.exist(table, "nominator_id", String.valueOf(userId)).equals("0")) {
            System.out.println("User: " + userId + " not found.");
        } else {
            try {
                String sql = "SELECT uname,uno FROM User WHERE uno IN (SELECT nominator_id FROM Election_Details WHERE nominee_id = '" + userId + "'";
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sql);
                ArrayList<String> arrString = new ArrayList<String>();
                if (rs.next() == false) {
                    System.out.println("User is not nominated ever.");
                } else {
                    rs.beforeFirst();
                    while (rs.next()) {
                        String name = rs.getString("uname");
                        String id = rs.getString("uno");
                        String cred = name + " " + id;
                        arrString.add(cred);
                    }
                }
                for (String str : arrString) {
                    System.out.println(str);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void printVotes(String screenname) {
        if (this.exist("Nominee", "nname", screenname).equals("0")) {
            System.out.println("User was never considered for promotion.");
        } else {
            try {
                Statement st = con.createStatement();
                String sql = "SELECT COUNT(vote) as numPos FROM Votes, Election_Details WHERE vote = 1 and nominee_id in (SELECT nno from Nominees where nname ='" + screenname + "'";
                ResultSet rs = st.executeQuery(sql);
                int vPos;
                while (rs.next()) {
                    vPos = rs.getInt("numPos");
                    System.out.println("Positive Votes: " + vPos);
                }
                st.close();
                rs.close();
                
                int nPos;
                String sql1 = "SELECT COUNT(vote) as neutral FROM Votes, Election_Details WHERE vote = 0 and nominee_id in (SELECT nno from Nominees where nname ='" + screenname + "'";
                Statement st1 = con.createStatement();
                ResultSet rs1 = st1.executeQuery(sql1);
                while (rs1.next()) {
                    nPos = rs.getInt("neutral");
                    System.out.println("Neutral Votes: " + nPos);
                }
                st1.close();
                rs1.close();
                
                String sql2 = "SELECT COUNT(vote) as negative FROM Votes, Election_Details WHERE vote = -1 and nominee_id in (SELECT nno from Nominees where nname ='" + screenname + "'";
                Statement st2 = con.createStatement();
                ResultSet rs2 = st2.executeQuery(sql2);
                int ntPos;
                while (rs2.next()) {
                    ntPos = rs.getInt("negative");
                    System.out.println("Negative Votes: " + ntPos);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String exist(String table, String row, Object value) {
        String ret = "0";
        try {
            Statement st = con.createStatement();
            st.executeQuery("select * from " + table + " where " + row + " = '" + value + "';");
            ResultSet rs = st.getResultSet();
            if (rs.next() == true) {
                ret = "1";
            }
        } catch (Exception e) {
            ret = "error";
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return ret;
    }

    public static void main(String[] args) {
        String file_name = "src/Wiki.txt";
        ReadFile file = null;
        try {
            file = new ReadFile(file_name);
            int numberOfLines = file.readLines();
            ArrayList<ArrayList<String>> aryLines = file.OpenFile(numberOfLines);
            //file.partition(numberOfLines, aryLines);
            /*
            for (String x : aryLines) {
                System.out.println(x);
            }
             */
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        file.connect();
        file.disconnect();
    }

    private String path;
    Connection con = null;
}
