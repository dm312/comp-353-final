/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package www.visitorpattern.com;

/**
 *
 * @author Danny
 */
public class GasCompany implements Utilities{

    @Override
    public void accept(BillVisitor bv) {
        bv.visit(this);
    }
    
}
