/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package www.visitorpattern.com;

/**
 *
 * @author Danny
 */
public class VisitorPatternDemo {
    public static void main(String[] args){
       
        Utilities waterCompany = new WaterCompany();
        
        ConcreteBillVisitor visitor3 = new ConcreteBillVisitor();
        waterCompany.accept(visitor3);
        
        Utilities billManager = new BillManager();
        ConcreteBillVisitor visitor = new ConcreteBillVisitor();
        billManager.accept(visitor);
        
        ConcreteBillVisitor visitor2 = new ConcreteBillVisitor();
        billManager.accept(visitor2);
    }
}
