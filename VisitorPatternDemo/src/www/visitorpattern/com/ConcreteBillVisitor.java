/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package www.visitorpattern.com;

/**
 *
 * @author Danny
 */
public class ConcreteBillVisitor implements BillVisitor {

    @Override
    public void visit(ElectricCompany ec) {
        System.out.println("Displaying Electric Company");
    }

    @Override
    public void visit(GasCompany gc) {
        System.out.println("Displaying Gas Company");

    }

    @Override
    public void visit(WaterCompany wc) {
        System.out.println("Displaying Water Company");

    }

    @Override
    public void visit(TelephoneCompany tc) {
        System.out.println("Displaying Telephone Company");

    }
    @Override
    public void visit(BillManager bm){
        System.out.println("Displaying Bill Manager");
    }

}
