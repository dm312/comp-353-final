/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package www.visitorpattern.com;

/**
 *
 * @author Danny
 */
public class BillManager implements Utilities {

    Utilities[] parts;

    public BillManager() {
        parts = new Utilities[]{new ElectricCompany(), new GasCompany(), new TelephoneCompany()};

    }
    @Override
    public void accept(BillVisitor bv){
        for(Utilities u : parts){
            u.accept(bv);
        }
        bv.visit(this);
    }
}
